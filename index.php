<?php

	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

	$method = $_SERVER['REQUEST_METHOD'];
	if ($method == "OPTIONS") {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
		header("HTTP/1.1 200 OK");
		die();
	}

	//
	//	Question2Answer API
	//	Author : Arun Anson
	//	Copyright (c) 2017 Hello Infinity Business Solutions Pvt. Ltd.
	//	15th June 2017
	//
	
	include 'settings.php';
	include 'login.php';
	include 'update-profile.php';
	include 'get-questions.php';
	include 'get-question-detail.php';
	include 'get-questions-answers.php';
	include 'create-question.php';
	include 'write-answer.php';
	include 'write-comment.php';
	include 'update-post.php';
	include 'update-posts.php';
	include 'view-profile.php';
	include 'get-users.php';
	include 'delete-post.php';
	include 'vote.php';
	include 'search.php';
	include 'check-vote.php';
	include 'get-user-questions.php';
	include 'get-tags.php';
	include 'set-best-answer.php';
	include 'save-image.php';
	include 'favorite.php';
	include 'get-user-favorites.php';
	include 'get-categories.php';
	include 'get-pages.php';
	include 'get-user-questions-byCategory.php';

	//Get JSON Request
	$json_request_ori = json_decode(file_get_contents('php://input'), true);
	$json_request["requestBody"] = $json_request_ori;

	$interactionCode	= $_GET['method'];
	$serviceId	= $_GET['serviceId'];

	$out = NULL;
	switch ($interactionCode) {

		case 'LOGIN':{
			$out = login($json_request, $serviceId);
		}	
		break;
		
		case 'UPDATEPROFILE':{
			$out = updateprofile($json_request);
		}
		break;

		case 'GETUSERS':{
			$out = get_users($json_request);
		}
		break;

		case 'GETQUESTIONS':{
			$out = get_questions($json_request);
		}
		break;

		case 'GETQUESTIONDETAIL':{
			$out = get_question_detail($json_request);
		}
		break;

		case 'GETQUESTIONSANDANSWERS':{
			$out = get_questions_answers($json_request);
		}
		break;

		case 'CREATEQUESTION':{
			$out = create_question($json_request);
		}
		break;

		case 'WRITEANSWER':{
			$out = write_answer($json_request);
		}
		break;

		case 'WRITECOMMENT':{
			$out = write_comment($json_request);
		}
		break;

		case 'UPDATEPOST':{
			$out = update_post($json_request);
		}
		break;

		case 'UPDATEANSWERS':{
			$out = update_posts($json_request);
		}
		break;

		case 'VIEWPROFILE':{
			$out = view_profile($json_request);
		}
		break;

		case 'DELETEPOST':{
			$out = delete_post($json_request);
		}
		break;

		case 'VOTE':{
			$out = vote($json_request);
		}
		break;

		case 'SEARCH':{
			$out = search($json_request);
		}
		break;

		case 'CHECKVOTE':{
			$out = check_vote($json_request);
		}
		break;

		case 'GETUSERQUESTIONS':{
			$out = get_user_questions($json_request);
		}
		break;

		case 'GETQUESTIONSANSWERSBYCATEGORY':{
			$out = get_questions_answers_category($json_request);
		}
		break;		

		case 'GETTAGS':{
			$out = get_tags($json_request);
		}
		break;

		case 'SETBESTANSWER':{
			$out = set_best_answer($json_request);
		}
		break;

		case 'SAVEIMAGE':{
			$out = save_image($json_request);
		}
		break;

		case 'FAVORITE':{
			$out = favorite($json_request);
		}
		break;

		case 'GETUSERFAVORITES':{
			$out = get_user_favorites($json_request);
		}
		break;

		case 'GETCATEGORIES':{
			$out = get_categories($json_request);
		}
		break;

		case 'GETPAGES':{
			$out = get_pages($json_request);
		}
		break;

		default:{
				$out['responseBody']['message'] = "Method Not Allowed";
				$out['responseBody']['method'] = $interactionCode;
				$out['responseHeader']['status'] = 405;
		}	
		break;
	}

	if (isset($out['responseHeader']['status'])) {
		http_response_code($out['responseHeader']['status']);	
	}

	header('Content-Type: application/json;charset=utf-8');

	print json_encode($out['responseBody'], JSON_UNESCAPED_SLASHES);

?>