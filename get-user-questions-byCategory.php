<?php

	//
	//	Question2Answer API
	//	Author : Arun Anson
	//	Copyright (c) 2017 Hello Infinity Business Solutions Pvt. Ltd.
	//	17th June 2017
	// 	GET QUESTIONS API
	// 	Gets all of the questions and its details in order they are posted.

	
	function get_questions_answers_category($json_request){

		include 'connection.php';

	
		$catid = isset($_GET['catid']) ? $_GET['catid'] : $json_request['requestBody']['catid'];

		$sql_get_question = "SELECT postid, parentid, title, type, userid, content, selchildid, UNIX_TIMESTAMP(updated) as updated, UNIX_TIMESTAMP(created) as created FROM ".TABLEPREFIX."posts WHERE categoryid='".$catid."' ORDER BY created DESC LIMIT 1000;";
		$result_get_questions = $conn->query($sql_get_question);

		while($row_get_questions= $result_get_questions->fetch_assoc()) {
            $data_get_questions[] = $row_get_questions;
        }

        $num_rows = mysqli_num_rows($result_get_questions);

		$out = [];
		if ($num_rows > 0) {
			foreach ($data_get_questions as $clave => $valor) {
				if ( $valor['type'] == 'Q' ) {
					$postid = $valor['postid'];
					$filteredArray = array_filter($data_get_questions, function($e) use(&$postid){
						return $e['type'] == 'A' && isset($e['parentid']) && $postid == $e['parentid'];
					});
					$thisQ['question'] = $valor;
					$thisQ['answers'] = [];
					
					if ( count(filteredArray) > 0 )
					foreach ($filteredArray as $clave2 => $valor2) {
						array_push($thisQ['answers'], $valor2);
					}
					
					array_push($out, $thisQ);
				}
				
			}
		}
		
		
        if ($num_rows > 0) {

			//success
			
			$res['responseHeader']['status'] = 200;
			$res['responseBody']['results'] = $out;
			$res['responseBody']['total'] = count($out);
		}else{

			//error
			
			$res['responseHeader']['status'] = 200; 
			$res['responseBody']['results'] = [];
			$res['responseBody']['total'] = 0;
		}


        //$json_response = json_encode($res, JSON_UNESCAPED_SLASHES);
		//echo $json_response;

		return $res;

	}
?>