<?php

	//Q2A Location on server
	define("Q2ALOCATION", "..");

	//API LIVE URL
	define("APILIVEURL", "http://renalbiomed.com/api/");

	//Table Prefix
	define("TABLEPREFIX", "qa_");

	//Mysql Database Connection
	define('Q2A_MYSQL_HOSTNAME', '{{Q2A_MYSQL_HOSTNAME}}');
	define('Q2A_MYSQL_USERNAME', '{{Q2A_MYSQL_USERNAME}}');
	define('Q2A_MYSQL_PASSWORD', '{{Q2A_MYSQL_PASSWORD}}');
	define('Q2A_MYSQL_DATABASE', '{{Q2A_MYSQL_DATABASE}}');

?>