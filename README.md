Login / Signup / Resigtation API
---------------------------------

Registers a user and signs him/her up using the social login.


__POST:__ /api/index.php?__method=LOGIN__

BODY:

```json
{
	"email": "anoop@helloinfinity.com",
	"identifier": "akm1kskdjbgasane",
	"username": "anoopanson",
	"source": "facebook"
}
```

RESPONSE:

```json
{
    "username":"anoopanson",
    "userid":"4"
}
```


GET QUESTIONS API
------------------

Gets all of the questions and its details (logged-in as __userid__) in order they are posted.

__GET:__ /api/index.php?__method=GETQUESTIONS&userid=1__

RESPONSE:

```json
{
	"questions": [{
		"title": "question with image test",
		"userid": "1",
		"postid": "27",
		"acount": "0",
		"views": "1",
		"content": "<p><img alt=\"\" src=\"https://www.w3schools.com/css/img_fjords.jpg\" style=\"height:400px; width:600px\">image goes here with text</p>",
		"tags": "image",
		"netvotes": "0",
		"updated": null,
		"created": "1500354649",
		"favorite": "27"
	}, {
		"title": "Question title1",
		"userid": "17",
		"postid": "21",
		"acount": "1",
		"views": "1",
		"content": "Question content",
		"tags": "tag1,tag2",
		"netvotes": "0",
		"updated": null,
		"created": "1498057186",
		"favorite": null
	}, {
		"title": "One test",
		"userid": "16",
		"postid": "15",
		"acount": "0",
		"views": "1",
		"content": "test content",
		"tags": "tag1,tag2",
		"netvotes": "0",
		"updated": null,
		"created": "1498039876",
		"favorite": null
	}, {
		"title": "One test",
		"userid": "16",
		"postid": "14",
		"acount": "0",
		"views": "1",
		"content": "test content",
		"tags": "tag1,tag2",
		"netvotes": "0",
		"updated": null,
		"created": "1498039791",
		"favorite": null
	}, {
		"title": "One test",
		"userid": "16",
		"postid": "13",
		"acount": "0",
		"views": "1",
		"content": "test content",
		"tags": "tag1,tag2",
		"netvotes": "0",
		"updated": null,
		"created": "1498039537",
		"favorite": null
	}, {
		"title": "One test",
		"userid": "16",
		"postid": "12",
		"acount": "0",
		"views": "0",
		"content": null,
		"tags": "",
		"netvotes": "0",
		"updated": null,
		"created": "1498039467",
		"favorite": null
	}, {
		"title": "test one",
		"userid": "16",
		"postid": "11",
		"acount": "0",
		"views": "1",
		"content": "test content",
		"tags": "test1",
		"netvotes": "0",
		"updated": null,
		"created": "1498039316",
		"favorite": null
	}, {
		"title": "How many movies have the RED Camera been used?",
		"userid": "1",
		"postid": "8",
		"acount": "0",
		"views": "1",
		"content": "In total, worldwide, how many movies have the camera RED been used?",
		"tags": "movie,camera",
		"netvotes": "-2",
		"updated": null,
		"created": "1497696372",
		"favorite": null
	}]
}
```

GET QUESTIONS AND ANSWERS
-------------------------

Gets all of the questions and its answers that user 'userid' has created.

__GET:__ /api/index.php?__method=GETQUESTIONSANDANWERS&userid=7__

RESPONSE:

```json
{
	"results": [{
		"question": {
			"postid": "105",
			"parentid": null,
			"title": "Questionnaires",
			"type": "Q",
			"userid": "7",
			"content": null,
			"selchildid": "129",
			"updated": null,
			"created": "1617711533"
		},
		"answers": [{
			"postid": "129",
			"parentid": "105",
			"title": null,
			"type": "A",
			"userid": "7",
			"content": "The response contained in this document are for informational purposes only.",
			"selchildid": null,
			"updated": null,
			"created": "1617711534"
		}]
	}, {
		"question": {
			"postid": "106",
			"parentid": null,
			"title": "RFPs",
			"type": "Q",
			"userid": "7",
			"content": null,
			"selchildid": "130",
			"updated": null,
			"created": "1617711533"
		},
		"answers": [{
			"postid": "130",
			"parentid": "106",
			"title": null,
			"type": "A",
			"userid": "7",
			"content": "While all reasonable care has been taken in compiling the response to the RFP, documents and details are presented in good faith and no warranty or guarantee (expressed or implied)...",
			"selchildid": null,
			"updated": null,
			"created": "1617711534"
		}]
	}],
	"total": 2
}
```

GET USERS
-------------------------

Update the "updated" field for specified "postids" for posts type 'A'

__GET:__ /api/index.php?__method=GETUSERS

RESPONSE:

```json
{
	"results": [{
		"userid": "1",
		"email": "admin@admin.com",
		"handle": "admin",
		"created": "1615456467"
	}, {
		"userid": "2",
		"email": "pepe@pepe.com",
		"handle": "pepe",
		"created": "1615460994"
	}],
	"total": 2
}
```

UPDATE DATE FOR ANSWERS
-------------------------

Update the "updated" field for specified "postids" for posts type 'A'

__POST:__ /api/index.php?__method=UPDATEANSWERS

BODY:

```json
{ 
	"postids": ["4", "5", "6"], 
	"updated": "1619517925" 
}
```


GET QUESTION DETAIL API
-----------------------

Gets the question and answers to a specific question (questionid) if logged-in as userid.

__GET:__: /api/index.php?__method=GETQUESTIONDETAIL&questionid=1&userid=2__

RESPONSE:

```json
{
	"question": [{
		"title": "update post title",
		"acount": "7",
		"userid": "1",
		"views": "1",
		"tags": "tag1 update, tag2 update1",
		"content": "update content",
		"selchildid": "16",
		"netvotes": "-1",
		"updated": null,
		"created": "1497455725",
		"answered": "yes"
	}],
	"answers": [{
		"title": null,
		"postid": "20",
		"userid": "1",
		"acount": "0",
		"views": "0",
		"tags": null,
		"content": "?one ?one ?one ?one ?one ?one ?one ?one",
		"netvotes": "0",
		"updated": null,
		"created": "1498057104"
	}, {
		"title": null,
		"postid": "19",
		"userid": "17",
		"acount": "0",
		"views": "0",
		"tags": null,
		"content": "here goes an another another answer",
		"netvotes": "0",
		"updated": null,
		"created": "1498056816"
	}, {
		"title": null,
		"postid": "18",
		"userid": "17",
		"acount": "0",
		"views": "0",
		"tags": null,
		"content": "here goes an another answer",
		"netvotes": "0",
		"updated": null,
		"created": "1498056770"
	}, {
		"title": null,
		"postid": "17",
		"userid": "17",
		"acount": "0",
		"views": "0",
		"tags": null,
		"content": "here goes an answer",
		"netvotes": "0",
		"updated": null,
		"created": "1498056746"
	}, {
		"title": null,
		"postid": "2",
		"userid": null,
		"acount": "0",
		"views": "0",
		"tags": null,
		"content": "answer test test",
		"netvotes": "-1",
		"updated": null,
		"created": "1497455825"
	}]
}
```

CREATE QUESTION API
--------------------

Creates a question for then loggedin user and returns the post id. if a user is not logged in, returns an error.

__POST:__ /api/index.php?__method=CREATEQUESTION__

BODY:

```json
{
	"userid": "16",
	"title": "One test",
	"content": "test content",
	"categoryid": "1",
	"tags": "tag1, tag2"
}
```

RESPONSE:

```json
{
    "userid":"16",
    "postid":15
}
```

WRITE ANSWER API
-----------------

Write an answer to a question for the loggedin user (userid) and returns the post id. if a user is not logged in, returns an error.

__POST:__ /api/index.php?__method=WRITEANSWER__

BODY:

```json
{ 
    "userid" : "2", 
    "content" : "test content", 
    "parentpostid" : "14" 
}
```

RESPONSE:

```json
{ 
    "userid":2,
    "postid":19 
}
```

WRITE COMMENT API
------------------

Write a comment to an answer for the loggedin user (userid) and returns the post id. if a user is not logged in, returns an error.

__POST:__ /api/index.php?__method=WRITECOMMENT__

BODY:

```json
{ 
    "userid" : "1", 
    "content" : "test content", 
    "parentpostid" : "1" 
}
```

RESPONSE:

```json
{
    "userid":"1",
    "postid":22
}
```

UPDATE PROFILE API
--------------------

Updates the user's email, location, fullname and avatar against a specified user_id. if the user_id is not specified, the API shows an error.

__POST:__ /api/index.php?__method=UPDATEPROFILE__

BODY:

```json
{
	"user_id": "1",
	"email_id": "anoop@helloinfinity.com",
	"location": "alappuzha",
	"full_name": "Anoop Anson",
	"avatar": "avatar-here"
}
```

RESPONSE:
```
200
```

VIEW PROFILE API
-----------------

View user's FullName, Username, Email, Location and avatar when userid is passed as an argument.

__GET:__: /api/index.php?__method=VIEWPROFILE&userid=1__


RESPONSE:

```json
{
	"user_fullname": "Anoop Anson",
	"user_location": "alappuzha",
	"user_name": "admin",
	"user_email": "anoop@helloinfinity.com",
	"user_avatarurl": "http://renalbiomed.com/api/avatar/1.png"
}
```


UPDATE POST API
-----------------

Update QUESTION, ANSWER, COMMENT or any other post type using postid

__POST:__ /api/index.php?__method=UPDATEPOST__

BODY:

```json
{
	"postid": "5",
	"postitle": "update post title 5",
	"postcontent": "update content5",
	"posttags": "tag1 update, tag2 update",
	"categoryid": "1"
}
```

RESPONSE:

```json
{
    "message":"Successfully updated!"
}
```


DELETE POST API
----------------

Delete QUESTION, ANSWER, COMMENT or any other post type using postid.

__POST:__ /api/index.php?__method=DELETEPOST__

BODY:

```json
{ 
    "postid" : "27" 
}
```

RESPONSE:

```json
{
    "message":"Successfully deleted!"
}
```

Voting API
-----------

Upvote or downvote a question. To upvote, specify 'vote' value as '1' and to downvote, specify 'vote' value as '0'

__POST:__ /api/index.php?__method=VOTE__

BODY:

```json
{ 
    "userid" : "1", 
    "postid" : "1", 
    "vote" : "1" 
}
```

RESPONSE:

```json
{
    "username":"anoopanson",
    "userid":"4"
}
```

Search API
-----------

Find questions with occurance of the given string.

__POST:__ /api/index.php?__method=SEARCH__

BODY:

```json
{ 
    "inquery" : "test", 
    "count" : "21", 
    "userid" : "1" 
}
```

RESPONSE:

```json
{
	"result": [{
		"title": "update post title",
		"postid": "1",
		"userid": "1",
		"acount": "7",
		"views": "1",
		"tags": "tag1 update, tag2 update",
		"netvotes": "1",
		"created": "1497455725"
	}, {
		"title": "One test",
		"postid": "15",
		"userid": "16",
		"acount": "0",
		"views": "1",
		"tags": "tag1,tag2",
		"netvotes": "0",
		"created": "1498039876"
	}, {
		"title": "One test",
		"postid": "13",
		"userid": "16",
		"acount": "0",
		"views": "1",
		"tags": "tag1,tag2",
		"netvotes": "0",
		"created": "1498039537"
	}, {
		"title": "test one",
		"postid": "11",
		"userid": "16",
		"acount": "0",
		"views": "1",
		"tags": "test1",
		"netvotes": "0",
		"created": "1498039316"
	}, {
		"title": "One test",
		"postid": "14",
		"userid": "16",
		"acount": "0",
		"views": "0",
		"tags": "tag1,tag2",
		"netvotes": "0",
		"created": "1498039791"
	}, {
		"title": "One test",
		"postid": "12",
		"userid": "16",
		"acount": "0",
		"views": "0",
		"tags": "",
		"netvotes": "0",
		"created": "1498039467"
	}]
}
```

CHECK VOTE API
---------------

Check if a user has voted for a specific postid. if voted, return stats.

__POST:__ /api/index.php?__method=CHECKVOTE__

BODY:

```json
{ 
    "user_id" : "1", 
    "post_id" : "21" 
}
```

RESPONSE:

```
204 - User hasn't voted for this post
```

GET USER QUESTIONS API
----------------------

Gets all of the questions and its details created by a specific user (user_id) in order they are posted.

__GET:__ /api/index.php?__method=GETUSERQUESTIONS&user_id=2__


RESPONSE:

```json
{
	"questions": [{
		"title": "How many movies have the RED Camera been used?",
		"userid": "1",
		"postid": "8",
		"acount": "0",
		"views": "1",
		"content": "In total, worldwide, how many movies have the camera RED been used?",
		"tags": "movie,camera",
		"netvotes": "0",
		"updated": null,
		"created": "1497696372"
	}, {
		"title": "23post title to update",
		"userid": "1",
		"postid": "5",
		"acount": "1",
		"views": "1",
		"content": "post content to update",
		"tags": "tag1, tag2",
		"netvotes": "0",
		"updated": null,
		"created": "1497694735"
	}, {
		"title": "update post title",
		"userid": "1",
		"postid": "1",
		"acount": "7",
		"views": "1",
		"content": "update content",
		"tags": "tag1 update, tag2 update1",
		"netvotes": "1",
		"updated": null,
		"created": "1497455725"
	}]
}
```

GET TAGS API
--------------------

Get tag suggestions like the one user entered.

__POST:__ /api/index.php?__method=GETTAGS__

BODY:

```json
{ 
    "keyword" : "ag" 
}
```

RESPONSE:

```json
{
	"questions": [{
		"wordid": "54",
		"word": "tag2",
		"titlecount": "0",
		"contentcount": "0",
		"tagwordcount": "4",
		"tagcount": "4"
	}, {
		"wordid": "53",
		"word": "tag1",
		"titlecount": "0",
		"contentcount": "0",
		"tagwordcount": "4",
		"tagcount": "4"
	}]
}
```

SET BEST ANSWER API
--------------------

Set best answer amoung the existing answers.

__POST:__ /api/index.php?__method=SETBESTANSWER__

BODY:

```json
{ 
    "questionid" : "1", 
    "answerid" : "2", 
    "userid" : "1" 
}
```

RESPONSE:

```
200
```

SAVE IMAGE API
----------------

Save images to server and return a url.

__POST:__ /api/index.php?__method=SAVEIMAGE__

BODY:

```json
{ 
    "base64data" : "1"
}
```

RESPONSE:

```json
{ 
    "Url":"http://renalbiomed.com/api/post/G3mRScNF.png"
}
```

FAVORITE SET/CLEAR API
-----------------------

Sets/clears a post item as favorite.

__POST:__ /api/index.php?__method=FAVORITE__

BODY:

```json
{
	"userid": "1",
	"posttype": "Q",
	"postid": "27",
	"favorite": "1"
}
```

RESPONSE:

```
200
```

GET USER FAVORITES API
-----------------------

Get the user's (userid) favourite posts.

__GET:__ /api/index.php?__method=GETUSERFAVORITES&userid=2__


RESPONSE:

```json
{
	"questions": [{
		"title": "question with image test",
		"userid": "1",
		"postid": "27",
		"acount": "0",
		"views": "1",
		"content": "<p><img alt=\"\" src=\"https://www.w3schools.com/css/img_fjords.jpg\" style=\"height:400px; width:600px\">image goes here with text</p>",
		"tags": "image",
		"netvotes": "0",
		"updated": null,
		"created": "1500354649",
		"favorite": "1"
	}, {
		"title": "update post title",
		"userid": "1",
		"postid": "1",
		"acount": "7",
		"views": "1",
		"content": "update content",
		"tags": "tag1 update, tag2 update1",
		"netvotes": "-1",
		"updated": null,
		"created": "1497455725",
		"favorite": "1"
	}]
}
```


GET CATEGORIES API
-------------------

Get categories and its associated details.

__GET__: /api/index.php?__method=GETCATEGORIES__


RESPONSE:

```json
{
	"categories": [{
		"categoryid": "1",
		"parentid": null,
		"title": "Category 1",
		"tags": "category-1",
		"content": "category 1 description",
		"qcount": "1",
		"position": "1",
		"backpath": "category-1"
	}, {
		"categoryid": "2",
		"parentid": null,
		"title": "Category 2",
		"tags": "category-2",
		"content": "Category 2 description",
		"qcount": "0",
		"position": "2",
		"backpath": "category-2"
	}]
}
```

GET PAGES API
--------------

Get pages and its associated details.

__GET__: /api/index.php?__method=GETPAGES__

RESPONSE:

```json
{
	"pages": [{
		"pageid": "2",
		"title": "Disclaimer",
		"nav": "_",
		"position": "1",
		"flags": "0",
		"permit": "150",
		"tags": "disclaimer",
		"heading": "Disclaimer",
		"content": "Disclaimers have a long legal history. They generally have two purposes:\r\n\r\nTo warn\r\nTo limit liability\r\nA warning sign is likely the earliest and easiest manifestation of a disclaimer.\r\n\r\nNo trespassing alerts passing individuals that they are near a private land boundary and also excuses the landowner of some liability if people visit uninvited.\r\n\r\nSometimes, the warning and limitation of liability are based on statutory law. For example, the state of Washington in the United States has a law that prevents people injured at equestrian facilities from pursuing legal damages.\r\n\r\nAny business that boards, trains or allows the riding of horses has to have a specific sign to enjoy this protection from liability. This sign acts as a disclaimer much like a No trespassing sign in that it informs and specifies limits on facility responsibilities"
	}]
}
```