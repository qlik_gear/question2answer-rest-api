<?php

	//
	//	Question2Answer API
	//	Author : Arun Anson
	//	Copyright (c) 2017 Hello Infinity Business Solutions Pvt. Ltd.
	//	17th June 2017
	// 	GET QUESTIONS API
	// 	Gets all of the questions and its details in order they are posted.

	// 	Sample Input
	// { "requestHeader": { "serviceId":"111", "interactionCode":"GETQUESTIONDETAIL"}, "requestBody" : { "questionid" : "1", "user_id" : "user" }}

	// 	Sample Output
	//	{"responseHeader":{"serviceId":"111","status":200},"responseBody":{"question":[{"title":"update post title","acount":"7","userid":"1","views":"1","tags":"tag1 update, tag2 update1","content":"update content","selchildid":"16","netvotes":"-1","updated":null,"created":"1497455725","answered":"yes"}],"answers":[{"title":null,"postid":"20","userid":"1","acount":"0","views":"0","tags":null,"content":"?one ?one ?one ?one ?one ?one ?one ?one","netvotes":"0","updated":null,"created":"1498057104"},{"title":null,"postid":"19","userid":"17","acount":"0","views":"0","tags":null,"content":"here goes an another another answer","netvotes":"0","updated":null,"created":"1498056816"},{"title":null,"postid":"18","userid":"17","acount":"0","views":"0","tags":null,"content":"here goes an another answer","netvotes":"0","updated":null,"created":"1498056770"},{"title":null,"postid":"17","userid":"17","acount":"0","views":"0","tags":null,"content":"here goes an answer","netvotes":"0","updated":null,"created":"1498056746"},{"title":null,"postid":"16","userid":"17","acount":"0","views":"0","tags":null,"content":"answer test content","netvotes":"0","updated":"1500285887","created":"1498056517"},{"title":null,"postid":"9","userid":"1","acount":"0","views":"0","tags":null,"content":"Answer2 on the test question","netvotes":"0","updated":null,"created":"1497697943"},{"title":null,"postid":"2","userid":null,"acount":"0","views":"0","tags":null,"content":"answer test test","netvotes":"-1","updated":null,"created":"1497455825"}]}}
	
	function get_questions_answers($json_request){

		include 'connection.php';

	
		$userid = isset($_GET['userid']) ? $_GET['userid'] : $json_request['requestBody']['userid'];
	
		//Set answered flag to no
		$answered = 'no';

		$sql_get_question = "SELECT postid, parentid, title, type, userid, content, selchildid, UNIX_TIMESTAMP(updated) as updated, UNIX_TIMESTAMP(created) as created FROM ".TABLEPREFIX."posts WHERE userid='".$userid."' ORDER BY created DESC LIMIT 1000;";
		$result_get_questions = $conn->query($sql_get_question);

		while($row_get_questions= $result_get_questions->fetch_assoc()) {
            $data_get_questions[] = $row_get_questions;
        }

        $num_rows = mysqli_num_rows($result_get_questions);

		$out = [];
		if ($num_rows > 0) {
			foreach ($data_get_questions as $clave => $valor) {
				if ( $valor['type'] == 'Q' ) {
					$postid = $valor['postid'];
					$filteredArray = array_filter($data_get_questions, function($e) use(&$postid){
						return $e['type'] == 'A' && isset($e['parentid']) && $postid == $e['parentid'];
					});
					$thisQ['question'] = $valor;
					$thisQ['answers'] = [];
					
					if ( count(filteredArray) > 0 )
					foreach ($filteredArray as $clave2 => $valor2) {
						array_push($thisQ['answers'], $valor2);
					}
					
					array_push($out, $thisQ);
				}
				
			}
		}
		
		
        if ($num_rows > 0) {

			//success
			
			$res['responseHeader']['status'] = 200;
			$res['responseBody']['results'] = $out;
			$res['responseBody']['total'] = count($out);
		}else{

			//error
			
			$res['responseHeader']['status'] = 200; 
			$res['responseBody']['results'] = [];
			$res['responseBody']['total'] = 0;
		}


        //$json_response = json_encode($res, JSON_UNESCAPED_SLASHES);
		//echo $json_response;

		return $res;

	}
?>