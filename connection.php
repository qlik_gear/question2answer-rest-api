<?php

    //DB Connectivity
    $servername = Q2A_MYSQL_HOSTNAME; //"db";
    $username   = Q2A_MYSQL_USERNAME; //"qlik";
    $password   = Q2A_MYSQL_PASSWORD; //"Qlik1234";
    $dbname     = Q2A_MYSQL_DATABASE; //"knowledgebase";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    
     // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    
?>