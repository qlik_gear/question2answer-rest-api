<?php

	//
	//	Question2Answer API
	//	Author : Arun Anson
	//	Copyright (c) 2017 Hello Infinity Business Solutions Pvt. Ltd.
	//	3rd July 2017
	// 	UPDATE POST API
	// 	Update QUESTION, ANSWER, COMMENT or any other post type using postid

	// 	Sample Input
	// { "requestHeader": { "serviceId":"111", "interactionCode":"UPDATEPOST"}, "requestBody" : { "postid" : "1", "postitle" : "update post title", "postcontent" : "update content", "posttags" : "tag1 update, tag2 update", "categoryid" : "1" }}

	// 	Sample Output
	// 	{"responseHeader":{"serviceId":"111","status":200},"responseBody":{"message":"Successfully updated!"}}
	
	function update_posts($json_request){

		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
			$res['responseHeader']['status'] = 405;
			$res['responseBody'] = "Method Not Allowed";
			return $res;
		}

		include 'connection.php';

        require_once Q2ALOCATION.'/qa-include/qa-base.php';
        require_once Q2ALOCATION.'/qa-include/app/posts.php';

		
		$postids = $json_request['requestBody']['postids'];

		if (!is_array($postids)){
			$res['responseHeader']['status'] = 400;
			$res['responseBody'] = "postids should be an array";
			return $res;
		}

		//$type = $json_request['requestBody']['type'];
		$integerIDs = array_map(function($value) {
			return intval($value);
		}, $postids);
		$updated = $json_request['requestBody']['updated'];
		
		$newUpdated = date_create();
		date_timestamp_set($newUpdated, $updated);
		$newUpdated = date_format($newUpdated, 'Y-m-d H:i:s');

		$sql_update_post = "UPDATE qa_posts SET created='".$newUpdated."' WHERE type='A' AND postid IN (" . implode(',', $integerIDs) . ");";

		$result_update_post = $conn->query($sql_update_post);
		$num_rows = mysqli_affected_rows($conn);
		

        if ($num_rows > 0) {

			//success
			$status = 200;
		}else{

			//error
			$status = 200; 
		}

		
		$res['responseHeader']['status'] = $status;
		$res['responseBody']['rowsAffected'] = $num_rows;
		//$res['responseBody']['query'] = $sql_update_post;


        //$json_response = json_encode($res, JSON_UNESCAPED_SLASHES);
		//echo $json_response;

		return $res;

	}
?>